# Product Rating Analysis

## Overview

#### App definition

Takes a ​CSV​ file as input (identified with a file path or an input stream), calculates the rating statistics and returns the results in ​JSON format.<br>

    public ​String​ ​calculate​(java.io.InputStream in)

The following information is expected as an output result of calculations:
- Number of invalid lines at the input CSV file,
- Number of valid lines processed,
- Top 3 best rated products based on the average of the ratings,
- Top 3 worst rated products,
- Most rated product,
- Least rated product.

#### Input Format

The input is described in a CSV formatted file, see an example in this section. It contains 4 columns that identifies the buyer, shop, product and rating.
- The ​Buyer Id​ is sequence of alphanumeric characters that starts with a letter, e.g. ​buyer1;
- The ​Shop Id i​ s a sequence of alphanumeric characters that starts with a letter, e.g. shop1;
- The ​Product Id​ is a sequence of alphanumeric characters and hyphen (-) that starts with a letter and ends with hyphen and a numeric value, the numeric value at the end is in a range between 1 and 99, e.g. ​smart-tv-01, patagonia-32;
- The ​Rating​ is numeric value between 1 and 5 as a whole number, e.g. ​3 ​or 5​.

The ​CSV​ lines can be empty, or corrupted (not all the columns are present or formatted properly - according to the rules described above).

#### Output Format

    {
        "validLines" : 120,
        "invalidLines" : 8,
        "bestRatedProducts" : [
            "lights-02",
            "chain-01",
            "widetv-03"
        ],
        "worstRatedProducts" : [
            "pandora-01",
            "guess-01",
            "wifi-projector-01"
        ],
        "mostRatedProduct" : "smarttv-01",
        "lessRatedProduct" : "saddle-01"
    }

## Packages

**.analyzer** -  **RatingAnalyzer** - main class, taking a stream of strings as input, and returning the Collected Rating results (**RatingStatistics**).<br>
**.mapper** -    builds **ProductRating** POJO from a string line<br>
**.model** -     defines **ProductRating** POJO<br>
**.report** -    extracts **ProductRatingReport** object<br>
**.stats** -     **RatingStatistics** collector in order to collect the final results<br>
**.util** -      **SortedList** collects N best/worst results depending on Comparator as parameter<br>
**.validator** - executes the validation of **ProductRating** POJO<br>

**RatingAnalyzeReader** builds stream of lines for **RatingAnalyzer**, and transforms the result into a report, then returning
    JSON object.<br>

**RatingAnalyzeApp** takes the "data.csv" file from *resources*, constructs the **RatingAnalyzeReader**, and executes
  the analyze.<br>

## Solution

The analyze is done in a single pass.<br>

First, the stream of lines is parsed, validated, and then collected with grouping collector, which produces the statistics per product.<br>
Second, these statistics per product are downstreamed and analyzed with the **RatingStatistics** collector.<br>
Then, the needed result is extracted, and a JSON-string is created from the report.<br>

**RatingStatistics** collector uses the **util.SortedList** to collect the best and worst rating results.<br>
**SortedList** maintains only N (=3, by default) results sorted, and therefore there is no need to sort the whole set of products.<br>

So, the complexity of the solution stays linear:<br>

    N = number of provided lines
    P = number of products <= number of provided lines
    O(N) + O(P) -> O(N).

## Validation

The given solution uses the bean validation by Hibernate, with the regular expressions to define the acceptable formats. <br>

However, a possible alternative validation would be a direct validation with the regular expressions without usage of bean validation libraries. <br>

## Enhancement with Java 12

The possible enhancement would be the use of *Collectors.teeing()* for combining the collectors for the stream termination.
However, [*Collectors.teeing()*](https://docs.oracle.com/en/java/javase/12/docs/api/java.base/java/util/stream/Collectors.html#teeing(java.util.stream.Collector,java.util.stream.Collector,java.util.function.BiFunction))
is available from Java 12 only, but the current implementation is with Java 11.<br>

That's why there is a central **stats.RatingStatistics** collector, collecting 6 required parameters in a single pass.

## Tests

There are the unit tests and end-2-end test, executed on the 'resources/test.csv' data.

## Build

mvn clean package

## Run

NB: use target/product-rating-1.0-SNAPSHOT-**fat**.jar

1. To execute on the resource 'data.csv', included into 'jar' (no parameters):

    java -jar target/product-rating-1.0-SNAPSHOT-fat.jar

2. To execute on a file \<**filepath**\>:

    java -jar target/product-rating-1.0-SNAPSHOT-fat.jar \<filepath\>

