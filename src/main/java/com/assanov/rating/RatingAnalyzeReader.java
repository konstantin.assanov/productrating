package com.assanov.rating;

import com.assanov.rating.analyzer.RatingAnalyzer;
import com.assanov.rating.report.ProductRatingsReport;
import com.assanov.rating.report.ReportExtractor;
import com.assanov.rating.stats.RatingStatistics;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.stream.Stream;

public class RatingAnalyzeReader {

    private final RatingAnalyzer analyzer;
    private final ReportExtractor extractor;
    private final ObjectMapper jsonMapper;

    public RatingAnalyzeReader(RatingAnalyzer analyzer,
                               ReportExtractor extractor,
                               ObjectMapper jsonMapper) {
        this.analyzer = analyzer;
        this.extractor = extractor;
        this.jsonMapper = jsonMapper;
    }

    public String calculate(InputStream in) throws JsonProcessingException {
        return toJson(extract(analyzer.analyze(streamOfLines(in))));
    }

    Stream<String> streamOfLines(InputStream in) {
        return new BufferedReader(new InputStreamReader(in)).lines();
    }

    ProductRatingsReport extract(RatingStatistics stats) {
        return extractor.extract(stats);
    }

    String toJson(ProductRatingsReport report) throws JsonProcessingException {
        return jsonMapper
                .writerWithDefaultPrettyPrinter()
                .writeValueAsString(report);
    }

}