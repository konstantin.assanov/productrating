package com.assanov.rating.stats;

import lombok.AllArgsConstructor;
import lombok.Data;

import static com.assanov.rating.model.InvalidProductRating.INVALID_PRODUCT_ID;

@Data
@AllArgsConstructor
public class Rating implements IRating {

    private final String id;
    private final long count;
    private final double rate;

    public boolean isInvalid() {
        return INVALID_PRODUCT_ID.equals(id);
    }

}
