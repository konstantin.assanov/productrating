package com.assanov.rating.stats;

public interface IRating {

    String getId();

    long getCount();

    double getRate();

    boolean isInvalid();

}