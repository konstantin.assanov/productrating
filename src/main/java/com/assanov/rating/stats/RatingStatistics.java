package com.assanov.rating.stats;

import com.assanov.rating.util.SortedList;

import java.util.Comparator;
import java.util.stream.Collector;

import lombok.Data;

@Data
public class RatingStatistics {

    private final SortedList<IRating> highest;

    private final SortedList<IRating> lowest;

    private IRating leastFrequent = null;

    private IRating mostFrequent = null;

    private long nbValid = 0;

    private long nbInvalid = 0;

    private RatingStatistics(int nbOfHighest,
                             int nbOfLowest) {
        final Comparator<IRating> comparator = Comparator.comparingDouble(IRating::getRate);
        lowest = new SortedList<>(comparator, nbOfLowest);
        highest = new SortedList<>(comparator.reversed(), nbOfHighest);
    }

    public void accept(IRating rating) {
        if (rating.isInvalid()) {
            nbInvalid += rating.getCount();
            return;
        }

        highest.add(rating);
        lowest.add(rating);

        if (leastFrequent == null) {
            leastFrequent = rating;
            mostFrequent = rating;
        }
        else {
            if (rating.getCount() < leastFrequent.getCount())
                leastFrequent = rating;
            else
                if (rating.getCount() > mostFrequent.getCount())
                    mostFrequent = rating;
        }

        nbValid += rating.getCount();
    }

    public RatingStatistics combine(RatingStatistics toAdd) {

        toAdd.highest.iterator().forEachRemaining(highest::add);
        toAdd.lowest.iterator().forEachRemaining(lowest::add);

        if (toAdd.leastFrequent != null)
            if (leastFrequent == null || toAdd.leastFrequent.getCount() < leastFrequent.getCount())
                leastFrequent = toAdd.leastFrequent;

        if (toAdd.mostFrequent != null)
            if (mostFrequent == null || toAdd.mostFrequent.getCount() > mostFrequent.getCount())
                mostFrequent = toAdd.mostFrequent;

        nbValid += toAdd.getNbValid();
        nbInvalid += toAdd.getNbInvalid();

        return this;
    }

    public static class Builder {

        private int nbOfHighest;
        private int nbOfLowest;

        public Builder(int nbOfHighest, int nbOfLowest) {
            this.nbOfHighest = nbOfHighest;
            this.nbOfLowest = nbOfLowest;
        }

        public Collector<IRating, RatingStatistics, RatingStatistics> build() {
            return Collector.of(
                    () -> new RatingStatistics(nbOfHighest, nbOfLowest),
                    RatingStatistics::accept,
                    RatingStatistics::combine,
                    Collector.Characteristics.UNORDERED, Collector.Characteristics.IDENTITY_FINISH
            );
        }
    }

}