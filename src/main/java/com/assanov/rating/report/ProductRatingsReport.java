package com.assanov.rating.report;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ProductRatingsReport {

    private final long validLines;
    private final long invalidLines;

    private final List<String> bestRatedProducts;
    private final List<String> worstRatedProducts;

    private final String mostRatedProduct;
    private final String lessRatedProduct;

    public ProductRatingsReport() {
        validLines = 0;
        invalidLines = 0;

        bestRatedProducts = List.of();
        worstRatedProducts = List.of();

        mostRatedProduct = null;
        lessRatedProduct = null;
    }

}