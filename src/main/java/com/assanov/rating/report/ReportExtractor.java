package com.assanov.rating.report;

import com.assanov.rating.stats.IRating;
import com.assanov.rating.stats.RatingStatistics;

import java.util.stream.Collectors;

public class ReportExtractor {

    public ProductRatingsReport extract(RatingStatistics stats) {
        return new ProductRatingsReport(
                stats.getNbValid(),
                stats.getNbInvalid(),
                stats.getHighest().list().stream().map(IRating::getId).collect(Collectors.toList()),
                stats.getLowest().list().stream().map(IRating::getId).collect(Collectors.toList()),
                stats.getMostFrequent().getId(),
                stats.getLeastFrequent().getId());
    }

}
