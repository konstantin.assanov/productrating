package com.assanov.rating.model;

public class InvalidProductRating extends ProductRating {

    public static final String INVALID_PRODUCT_ID = "100";

    private final ProductRating original;

    public InvalidProductRating(ProductRating original) {
        super(null, null, INVALID_PRODUCT_ID, 0);
        this.original = original;
    }

    public ProductRating getOriginal() {
        return original;
    }

}
