package com.assanov.rating.model;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Pattern;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ProductRating {

    @NotNull
    @Pattern(regexp = "^[A-Za-z][A-Za-z0-9]*$")
    private final String buyerId;

    @NotNull
    @Pattern(regexp = "^[A-Za-z][A-Za-z0-9]*$")
    private final String shopId;

    @NotNull
    @Pattern(regexp = "^[A-Za-z][A-Za-z0-9\\-]*-(0?[1-9]|[1-9][0-9])$")
    private final String id;        // product id

    @Min(1)
    @Max(5)
    @NotNull
    private final Integer rating;

}