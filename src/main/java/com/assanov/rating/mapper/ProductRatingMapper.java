package com.assanov.rating.mapper;

import com.assanov.rating.model.ProductRating;

public class ProductRatingMapper implements IDataMapper<ProductRating> {

    private final String delimiter;

    public ProductRatingMapper(String delimiter) {
        this.delimiter = delimiter;
    }

    @Override
    public ProductRating toModel(String line) {
        String[] fields = line.split(delimiter);
        return new ProductRating(
                get(fields, 0),
                get(fields, 1),
                get(fields, 2),
                parseInt(get(fields, 3)));
    }

    Integer parseInt(String val) {
        try {
            return Integer.parseInt(val);
        }
        catch (NumberFormatException e) {
            return null;
        }
    }

}