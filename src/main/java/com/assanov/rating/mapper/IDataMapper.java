package com.assanov.rating.mapper;

import java.util.Optional;

public interface IDataMapper<T> {

    T toModel(String line);

    default String get(String[] fields, int index) {
        if (fields == null || index >= fields.length)
            return null;

        return Optional
                .ofNullable(fields[index])
                .map(String::trim)
                .get();
    }

}