package com.assanov.rating.util;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

public class SortedList<E> {

    private final int capacity;
    private final List<E> best = new ArrayList<>();
    private final Comparator<E> comparator;

    public SortedList(Comparator<E> comparator, int capacity) {
        if (capacity < 1)
            throw new IllegalArgumentException("Capacity must be positive integer (> 1)");

        this.capacity = capacity;
        this.comparator = comparator;
    }

    public void add(E e) {
        best.add(e);
        best.sort(comparator);

        if (best.size() > capacity){
            best.remove(best.get(best.size() - 1));
        }
    }

    public int getCapacity() {
        return capacity;
    }

    public Iterator<E> iterator() {
        return best.iterator();
    }

    public List<E> list() {
        return List.copyOf(best);
    }

}
