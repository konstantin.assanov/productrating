package com.assanov.rating.analyzer;

import com.assanov.rating.mapper.IDataMapper;
import com.assanov.rating.model.ProductRating;
import com.assanov.rating.stats.IRating;
import com.assanov.rating.stats.Rating;
import com.assanov.rating.stats.RatingStatistics;
import com.assanov.rating.validator.IBasicValidator;

import java.util.IntSummaryStatistics;
import java.util.Map;
import java.util.stream.Stream;

import static java.util.stream.Collectors.groupingBy;
import static java.util.stream.Collectors.summarizingInt;

public class RatingAnalyzer {

    private final IDataMapper<ProductRating> dataMapper;
    private final IBasicValidator<ProductRating> validator;
    private final RatingStatistics.Builder toRatingStatisticsCollector;

    RatingAnalyzer(IDataMapper<ProductRating> dataMapper,
                   IBasicValidator<ProductRating> validator,
                   RatingStatistics.Builder toRatingStatisticsCollector) {
        this.dataMapper = dataMapper;
        this.validator = validator;
        this.toRatingStatisticsCollector = toRatingStatisticsCollector;
    }

    public RatingStatistics analyze(Stream<String> streamOfLines) {

        return streamOfLines
                // parallel() is optional
                .parallel()
                .map(dataMapper::toModel)
                .map(validator::validate)
                .collect(groupingBy(ProductRating::getId,
                                summarizingInt(ProductRating::getRating)))
                .entrySet()
                .stream()
                // parallel() is optional
                .parallel()
                .map(this::toRating)
                .collect(toRatingStatisticsCollector.build());

    }

    IRating toRating(Map.Entry<String, IntSummaryStatistics> e) {
        return new Rating(e.getKey(), e.getValue().getCount(), e.getValue().getAverage());
    }

}