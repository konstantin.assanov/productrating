package com.assanov.rating.analyzer;

import com.assanov.rating.mapper.IDataMapper;
import com.assanov.rating.mapper.ProductRatingMapper;
import com.assanov.rating.model.ProductRating;
import com.assanov.rating.stats.RatingStatistics;
import com.assanov.rating.validator.BasicProductRatingValidator;
import com.assanov.rating.validator.IBasicValidator;

import java.util.Optional;

public class RatingAnalyzerBuilder {

    private int nbOfBestRated = 3;
    private int nbOfWorstRated = 3;
    private IDataMapper<ProductRating> ratingMapper;
    private IBasicValidator<ProductRating> validator;

    public RatingAnalyzerBuilder() {}

    public RatingAnalyzerBuilder nbOfBestRated(int nbOfBestRated) {
        this.nbOfBestRated = nbOfBestRated;
        return this;
    }

    public RatingAnalyzerBuilder nbOfWorstRated(int nbOfWorstRated) {
        this.nbOfWorstRated = nbOfWorstRated;
        return this;
    }

    public RatingAnalyzerBuilder dataMapper(IDataMapper<ProductRating> ratingMapper) {
        this.ratingMapper = ratingMapper;
        return this;
    }

    public RatingAnalyzerBuilder validator(IBasicValidator<ProductRating> validator) {
        this.validator = validator;
        return this;
    }

    public RatingAnalyzer build() {
        return new RatingAnalyzer(
                Optional.ofNullable(ratingMapper)
                        .orElse(new ProductRatingMapper(",")),
                Optional.ofNullable(validator)
                        .orElse(new BasicProductRatingValidator.DefaultBuilder().build()),
                new RatingStatistics.Builder(nbOfBestRated, nbOfWorstRated));
    }

}