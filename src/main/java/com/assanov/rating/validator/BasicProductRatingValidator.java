package com.assanov.rating.validator;

import com.assanov.rating.model.InvalidProductRating;
import com.assanov.rating.model.ProductRating;

import javax.validation.Validation;
import javax.validation.Validator;

public class BasicProductRatingValidator implements IBasicValidator<ProductRating> {

    private final Validator beanValidator;

    public BasicProductRatingValidator(Validator beanValidator) {
        this.beanValidator = beanValidator;
    }

    public ProductRating validate(ProductRating productRating) {
        if (beanValidator.validate(productRating).isEmpty())
            return productRating;
        else
            return new InvalidProductRating(productRating);
    }

    public static class DefaultBuilder {

        public DefaultBuilder() {}

        public BasicProductRatingValidator build() {
            return new BasicProductRatingValidator(
                    Validation
                    .buildDefaultValidatorFactory()
                    .getValidator());
        }

    }

}
