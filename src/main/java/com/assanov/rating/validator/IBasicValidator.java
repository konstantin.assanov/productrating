package com.assanov.rating.validator;

public interface IBasicValidator<T> {

    T validate(T data);

}
