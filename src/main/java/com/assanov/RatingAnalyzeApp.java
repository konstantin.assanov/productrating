package com.assanov;

import com.assanov.rating.analyzer.RatingAnalyzer;
import com.assanov.rating.RatingAnalyzeReader;
import com.assanov.rating.analyzer.RatingAnalyzerBuilder;
import com.assanov.rating.report.ReportExtractor;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class RatingAnalyzeApp {

    private InputStream fin(String filename) throws FileNotFoundException {
        if (filename != null)
            return new FileInputStream(filename);

        return getClass().getResourceAsStream("/data.csv");
    }

    public static void main(String[] args) {

        String filename = null;
        if (args.length > 0)
            filename = args[0];

        RatingAnalyzer analyzer = new RatingAnalyzerBuilder().build();

        RatingAnalyzeReader ratingReader = new RatingAnalyzeReader(analyzer, new ReportExtractor(), new ObjectMapper());

        try (InputStream fin = new RatingAnalyzeApp().fin(filename)) {

            String json = ratingReader.calculate(fin);

            System.out.println("JSON result:\n" + json);

        } catch (FileNotFoundException fnfe) {

            System.out.println("File has not been found: " + fnfe.getMessage());

        } catch (IOException ioe) {

            System.out.println("I/O exception: " + ioe.getMessage());

        }

    }

}