package com.assanov.rating.mapper;

import com.assanov.rating.model.ProductRating;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.spy;

public class ProductRatingMapperTest {

    ProductRatingMapper mapper;

    @BeforeEach
    void init() {
        mapper = new ProductRatingMapper(",");
    }

    @Test
    void shouldReturnNullIfNullInputString() {
        Integer val = mapper.parseInt(null);

        assertNull(val);
    }

    @Test
    void shouldReturnNullIfEmptyInputString() {
        Integer val = mapper.parseInt("  ");

        assertNull(val);
    }

    @Test
    void shouldReturnNullIfInvalidInputString() {
        Integer val = mapper.parseInt("antoine");

        assertNull(val);
    }

    @Test
    void shouldReturnNullIfCorrectNumberString() {
        Integer val = mapper.parseInt("451");

        assertEquals(451, val);
    }

    @Test
    void shouldReturnNullIfNegativeNumberString() {
        Integer val = mapper.parseInt("-451");

        assertEquals(-451, val);
    }

    @Test
    void shouldBuildCorrectDataSuccessfully() {
        ProductRatingMapper spy = spy(mapper);

        doReturn("field0").when(spy).get(any(String[].class), eq(0));
        doReturn("field1").when(spy).get(any(String[].class), eq(1));
        doReturn("field2").when(spy).get(any(String[].class), eq(2));

        doReturn(33).when(spy).parseInt(eq("33"));
        doReturn("33").when(spy).get(any(String[].class), eq(3));

        ProductRating productRating = spy.toModel("");

        assertEquals("field0", productRating.getBuyerId());
        assertEquals("field1", productRating.getShopId());
        assertEquals("field2", productRating.getId());
        assertEquals(33, productRating.getRating());

    }

}