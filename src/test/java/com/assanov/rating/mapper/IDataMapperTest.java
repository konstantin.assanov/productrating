package com.assanov.rating.mapper;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import static org.junit.jupiter.api.Assertions.*;

public class IDataMapperTest {

    IDataMapper mapper = Mockito.mock(IDataMapper.class, Mockito.CALLS_REAL_METHODS);

    @Test
    void shouldReturnNullIfNullFieldsArray() {
        String field = mapper.get(null, 2);

        assertNull(field);
    }

    @Test
    void shouldReturnNullIfEmptyFieldsArray() {
        String field = mapper.get(new String[]{}, 0);

        assertNull(field);
    }

    @Test
    void shouldReturnNullIfIndexOutOfBounds() {
        String field = mapper.get(new String[]{"a", "b"}, 2);

        assertNull(field);
    }

    @Test
    void shouldReturnTrimmedFieldIfIndexInBounds() {
        String field = mapper.get(new String[]{"  a ", " b "}, 1);

        assertEquals("b", field);
    }

}
