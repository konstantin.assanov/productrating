package com.assanov.rating.validator;

import com.assanov.rating.model.InvalidProductRating;
import com.assanov.rating.model.ProductRating;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validator;

import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class BasicProductRatingValidatorTest {

    Validator beanValidator;
    BasicProductRatingValidator productRatingValidator;

    @BeforeEach
    void init() {
        beanValidator = mock(Validator.class);
        productRatingValidator = new BasicProductRatingValidator(beanValidator);
    }

    @Test
    void shouldReturnOriginalData() {
        ProductRating valid = mock(ProductRating.class);

        when(beanValidator.validate(valid)).thenReturn(Set.of());

        assertEquals(valid, productRatingValidator.validate(valid));
    }

    @Test
    void shouldReturnInvalidProductRating() {
        ProductRating invalid = mock(ProductRating.class);

        when(beanValidator.validate(invalid)).thenReturn(Set.of(mock(ConstraintViolation.class)));

        ProductRating validated = productRatingValidator.validate(invalid);

        assertTrue(validated instanceof InvalidProductRating);

    }

}
